## Synopsis

This project contains configuration files for Elasticsearch, Logstash and Kibana. 

## Motivation

Created to assist users setting up their infrastructure with functioning configuration files they can use as an example. 

## Installation

Provide code examples and explanations of how to get the project.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Jake Strati

## License

Free to share and distribute. Please leave credits in config files.